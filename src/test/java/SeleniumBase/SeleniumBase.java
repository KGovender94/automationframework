package SeleniumBase;

        import PropertiesLoader.PropertiesLoader;
        import org.openqa.selenium.WebDriver;
        import org.openqa.selenium.chrome.ChromeDriver;
        import org.testng.Assert;
        import org.testng.annotations.*;

        import static ReportOutput.ReporterOutput.ReporterLog;

public class SeleniumBase {

    public static ChromeDriver CHROME_DRIVER;
    private final PropertiesLoader propertiesLoader = new PropertiesLoader();

    public SeleniumBase() {
        switch (System.getProperty("os.name")) {
            case "Windows 10":
                System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
                ReporterLog("Setting driver up for windows system");
                break;

            default:
                Assert.fail("Unable to find OS properties : " + System.getProperty("os.name"));
        }
    }

    public WebDriver getDriver() {
        CHROME_DRIVER = new ChromeDriver();
        CHROME_DRIVER.manage().window().maximize();
        return CHROME_DRIVER;
    }

    public void goToURL(String url) {
        CHROME_DRIVER.get(url);
    }

/*    @BeforeSuite
    public void setup() {
        getDriver();
    }*/

    @AfterTest
    public void tearDown() {
        if (null != CHROME_DRIVER)
            CHROME_DRIVER.close();
        CHROME_DRIVER.quit();
    }

    @BeforeTest(alwaysRun = true)
    @Parameters({"environment"})
    public void setupEnvironment(String environment) {
        propertiesLoader.loadProperties();
        propertiesLoader.setProperties("environment", environment);
    }


    public SeleniumBase getSeleniumBase() {
        return this;
    }

}
