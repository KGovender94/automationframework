package POMClasses;

import BaseClasses.PomBase;
import SeleniumBase.SeleniumBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LandingPage extends PomBase {
    WebDriver driver;

    public LandingPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(how = How.XPATH, using = "//h1[contains(text(),'Welcome to Address Book')]")
    private WebElement txtWelcome;

    @FindBy(how = How.XPATH, using = "//h4[contains(text(),'A simple web app for showing off your testing')]")
    private WebElement txtSubHeading;

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Addresses')]")
    private WebElement menuAddresses;

    @FindBy(how = How.XPATH, using = "//a[@id='sign-in']")
    private WebElement btnSignIn;

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Sign out')]")
    private WebElement btnSignOut;

    public boolean GoToPage(String environment, SeleniumBase seleniumBase) {
        seleniumBase.goToURL(getBaseUrl(environment));
        return true;
    }

    public boolean goToLandingPage(String environment, SeleniumBase seleniumBase) {
        GoToPage(environment, seleniumBase);
        return true;
    }

    public void clickOnSignInButton() {
        clickElement(btnSignIn);
    }

    public void clickOnSignOutButton(){
        if (btnSignOut.isDisplayed()){
            clickElement(btnSignOut);
        }
    }

    public void clickOnAddressTab(){
        clickElement(menuAddresses);
    }
}
