package POMClasses;

import BaseClasses.PomBase;
import SeleniumBase.SeleniumBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class NewAddressPage extends PomBase {
    WebDriver driver;

    public NewAddressPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(how = How.XPATH, using = "//h2[contains(text(),'New Address')]")
    private WebElement txtAddresses;

    @FindBy(how = How.XPATH, using = "//input[@id='address_first_name']")
    private WebElement txtBoxFirstName;

    @FindBy(how = How.XPATH, using = "//input[@id='address_last_name']")
    private WebElement txtBoxLastName;

    @FindBy(how = How.XPATH, using = "//input[@id='address_street_address']")
    private WebElement txtBoxAddressline1;

    @FindBy(how = How.XPATH, using = "//input[@id='address_secondary_address']")
    private WebElement txtBoxAddressline2;

    @FindBy(how = How.XPATH, using = "//input[@id='address_city']")
    private WebElement txtBoxCity;

    @FindBy(how = How.CSS, using = "#address_state")
    private WebElement dropDown;

    @FindBy(how = How.XPATH, using = "//input[@id='address_zip_code']")
    private WebElement txtBoxZip;

    @FindBy(how = How.XPATH, using = "//input[@id='address_country_us']")
    private WebElement optionUS;

    @FindBy(how = How.XPATH, using = "//input[@id='address_birthday']")
    private WebElement selectBirthdayDate;

    @FindBy(how = How.XPATH, using = "//input[@id='address_age']")
    private WebElement txtBoxAge;

    @FindBy(how = How.XPATH, using = "//input[@id='address_website']")
    private WebElement txtBoxWebsite;

    @FindBy(how = How.XPATH, using = "//input[@id='address_phone']")
    private WebElement txtBoxPhoneNumber;

    @FindBy(how = How.XPATH, using = "//input[@id='address_interest_climb']")
    private WebElement chkBoxClimbing;

    @FindBy(how = How.XPATH, using = "//body/div[1]/div[1]/div[1]/form[1]/div[17]/input[1]")
    private WebElement btnCreateAddress;


    public void enterNewAddress(String firstName, String lastName, String addressLine1, String addressline2, String city){
        captureKeys(txtBoxFirstName,firstName);
        captureKeys(txtBoxLastName,lastName);
        captureKeys(txtBoxAddressline1,addressLine1);
        captureKeys(txtBoxAddressline2,addressline2);
        captureKeys(txtBoxCity,city);
    }

    public void selectCountry(){
        clickElement(optionUS);
    }

    public void enterNerPersonalDetails(String age, String website, String phone){
        captureKeys(txtBoxAge,age);
        captureKeys(txtBoxWebsite,website);
        captureKeys(txtBoxPhoneNumber,phone);
    }

    public void CreateNewAddress(){
        clickElement(btnCreateAddress);
    }

    }

