package POMClasses;

import BaseClasses.PomBase;
import SeleniumBase.SeleniumBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class AddressesPage extends PomBase {
    WebDriver driver;

    public AddressesPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(how = How.XPATH, using = "//h2[contains(text(),'Addresses')]")
    private WebElement txtAddresses;

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'New Address')]")
    private WebElement linkNewAddresses;

    @FindBy(how = How.XPATH, using = "//a[@id='sign-in']")
    private WebElement btnSignIn;

    public boolean GoToPage(String environment, SeleniumBase seleniumBase) {
        seleniumBase.goToURL(getBaseUrl(environment));
        return true;
    }

    public boolean goToLandingPage(String environment, SeleniumBase seleniumBase) {
        GoToPage(environment, seleniumBase);
        return true;
    }

    public void clickOnSignInButton(){
        clickElement(btnSignIn);
    }

    public void clickNewAddressLink(){
        clickElement(linkNewAddresses);
    }


}
