package POMClasses;

import BaseClasses.PomBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class SignInPage extends PomBase {
    WebDriver driver;

    public SignInPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(how = How.XPATH, using = "//h2[contains(text(),'Sign in')]")
    private WebElement txtSignIn;

    @FindBy(how = How.XPATH, using = "//input[@id='session_email']")
    private WebElement txtboxSignInEmail;

    @FindBy(how = How.XPATH, using = "//input[@id='session_password']")
    private WebElement txtboxSignInPassword;

    @FindBy(how = How.XPATH, using = "//body/div[1]/div[1]/div[1]/div[1]/form[1]/div[3]/input[1]")
    private WebElement btnSignIn;

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Sign up')]")
    private WebElement btnSignUp;

    public void enterLoginDetails(String email, String password){
        captureKeys(txtboxSignInEmail,"Tim@email.com");
        captureKeys(txtboxSignInPassword,"12345");
    }

    public void clickOnLoginButton(){
        clickElement(btnSignIn);
    }

    public void clickOnSignUpButton(){
        clickElement(btnSignUp);
    }


}
