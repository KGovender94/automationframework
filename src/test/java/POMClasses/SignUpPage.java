package POMClasses;

import BaseClasses.PomBase;
import Utils.FileWriter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class SignUpPage extends PomBase {
    WebDriver driver;

    public SignUpPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(how = How.XPATH, using = "//h2[contains(text(),'Sign in')]")
    private WebElement txtSignIn;

    @FindBy(how = How.XPATH, using = "//input[@id='user_email']")
    private WebElement txtboxUserEmail;

    @FindBy(how = How.XPATH, using = "//input[@id='user_password']")
    private WebElement txtboxUserPassword;

    @FindBy(how = How.XPATH, using = "//body/div[1]/div[1]/div[1]/div[1]/form[1]/div[3]/input[1]")
    private WebElement btnSignUpUser;

    @FindBy(how = How.XPATH, using = "//body/div[1]/div[1]/div[1]/div[1]/form[1]/div[4]/a[1]")
    private WebElement btnBackToSigIn;

    public void enterUserSignUpDetails(String email, String password){
        captureKeys(txtboxUserEmail,email);
        captureKeys(txtboxUserPassword,password);
    }

    public void clickOnSignUpUserButton(){
        clickElement(btnSignUpUser);
    }

    public void clickOnBackToLoginButton(){
        clickElement(btnBackToSigIn);
    }

}
