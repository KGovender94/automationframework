package Tests;

import POMClasses.*;
import SeleniumBase.SeleniumBase;
import com.github.javafaker.Faker;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.Locale;

import static ReportOutput.ReporterOutput.ReporterLog;

public class GoToLandingPageTest extends SeleniumBase {

    private String environment;
    WebDriver driver;
    LandingPage landingPage;
    SignInPage signInPage;
    SignUpPage signUpPage;
    AddressesPage addressesPage;
    NewAddressPage newAddressPage;

    Faker dataFaker = new Faker(new Locale("en-ZA"));
    String email = dataFaker.internet().emailAddress();
    String firstName = dataFaker.name().firstName();
    String lastName = dataFaker.name().lastName();
    String addressLine1 = dataFaker.address().streetAddress();
    String addressLine2 = dataFaker.address().buildingNumber();
    String city = dataFaker.address().city();
    String age = dataFaker.number().digit();
    String website = dataFaker.internet().domainName();
    String phone = dataFaker.phoneNumber().cellPhone();


    public GoToLandingPageTest() {
        driver = getDriver();
        landingPage = new LandingPage(driver);
        signUpPage = new SignUpPage(driver);
        signInPage = new SignInPage(driver);
        addressesPage  = new AddressesPage(driver);
        newAddressPage = new NewAddressPage(driver);

    }

    @BeforeTest()
    @Parameters({"environment"})
    public void setup(String environment) {
        this.environment = environment;
    }

    @Test(priority = 0)
    public void Navigate_To_Landing_Page() {
        landingPage.goToLandingPage(environment, getSeleniumBase());
        ReporterLog("Navigated to Address Book Site");
    }

    @Test(priority = 1)
    public void NavigateToSignIn() {
        Navigate_To_Landing_Page();
        landingPage.clickOnSignInButton();
        ReporterLog("navigated to Sign in");
    }

    @Test(priority = 2)
    public void SignUpNewUser() {
        NavigateToSignIn();
        signInPage.clickOnSignUpButton();
        signUpPage.enterUserSignUpDetails(email,"12345");
        signUpPage.clickOnSignUpUserButton();
        ReporterLog("Signed up new User");
    }

    @Test(priority = 3,enabled = true)
    public void CreateNewAddress() {
        ReporterLog("Create new Address");
        SignUpNewUser();
        landingPage.clickOnAddressTab();
        addressesPage.clickNewAddressLink();
        newAddressPage.enterNewAddress(firstName,lastName,addressLine1,addressLine2,city);
        newAddressPage.enterNerPersonalDetails(age,website,phone);
        newAddressPage.selectCountry();
        newAddressPage.CreateNewAddress();
    }
}
