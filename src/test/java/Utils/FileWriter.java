package Utils;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class FileWriter {
    private static final String CHARACTER_ENCODING = "UTF-8";
    public FileWriter() {
        //Intentionally left blank
    }

    public void email(String refNumber) {
        String logger;
        try {
            PrintWriter writer = new PrintWriter("src//test//resources//data//email.properties", CHARACTER_ENCODING);
            logger = "referenceNumber = " + refNumber;
            writer.println(logger);
            writer.close();
        } catch (UnsupportedEncodingException | FileNotFoundException var5) {
            var5.printStackTrace();
        }

    }
}