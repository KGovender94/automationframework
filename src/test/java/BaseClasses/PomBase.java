package BaseClasses;

import PropertiesLoader.PropertiesLoader;
import ReportOutput.ReporterOutput;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import static SeleniumBase.SeleniumBase.CHROME_DRIVER;

public class PomBase {

    private final PropertiesLoader propertiesLoader = new PropertiesLoader();
    private final int timeOut = 60;

    public String getBaseUrl(String environment) {
        String url = "";
        switch (environment.toLowerCase()) {
            case "prod":
                url = propertiesLoader.getProperty("application.url.test");
                break;
            default:
                Assert.fail(environment + " is invalid");
        }
        return url;
    }

    private void waitForElementXPath(WebElement elementValue) {
        new WebDriverWait(CHROME_DRIVER, timeOut).until(ExpectedConditions.visibilityOf(elementValue));
        ReporterOutput.ReporterLog("Found element " + elementValue);
    }

    public void captureKeys(WebElement element, String text) {
        waitForElementXPath(element);
        element.sendKeys(text);
    }

    public void clickElement(WebElement element){
        waitForElementXPath(element);
        element.click();
    }
}